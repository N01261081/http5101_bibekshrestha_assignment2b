﻿<%@ Page Title="DataBase" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DB.aspx.cs" Inherits="Assign2b.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="trickyConcept" runat="server">
    <h2>SQL Aggregate Functions</h2>
    <p>
        Aggregate functions return a single result row based on groups of rows, rather than on single rows. 
        Aggregate functions can appear in select lists and in ORDER BY and HAVING clauses. 
        They are commonly used with the GROUP BY clause in a SELECT statement, 
        where Oracle Database divides the rows of a queried table or view into groups. 
        In a query containing a GROUP BY clause, the elements of the select list can be aggregate functions, 
        GROUP BY expressions, constants, or expressions involving one of these. 
        Oracle applies the aggregate functions to each group of rows and returns a single result row for each group.
    </p>
    <p>
        If you omit the GROUP BY clause, then Oracle applies aggregate functions in the select list to all the rows in the queried table or view. 
        You use aggregate functions in the HAVING clause to eliminate groups from the output based on the results of the aggregate functions, 
        rather than on the values of the individual rows of the queried table or view.
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="myCode" runat="server">
    <p>This code block below is practice commands for the database course.</p>
    <uc:CodeBox runat="server" id="my_db_code" Code="db" Owner="me" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="someCode" runat="server">
    <p>This code block below is extracted from 
        <a href="https://docs.oracle.com/database/121/SQLRF/functions046.htm#SQLRF00624">
            Oracle DOCS
        </a>. It calcualates for each employee in the employees table, 
        the moving count of employees earning salaries in the range 50 less than through 150 greater than the employee's salary.</p>
    
    <uc:CodeBox runat="server" id="some_db_code" Code="db" Owner="other" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="list" runat="server">
    <h3>Useful links</h3>
    <ul>
        <li><a href="https://www.w3schools.com/sql/sql_intro.asp" target="_blank">W3SCHOOLS</a></li>
        <li><a href="https://www.tutorialspoint.com/sql/sql-select-database.htm" target="_blank">Tutorials Point</a></li>
        <li><a href="http://www.sqlcourse.com/intro.html" target="_blank">SQL Course</a></li>
        <li><a href="https://docs.oracle.com/cd/B19306_01/server.102/b14200/toc.htm" target="_blank">Oracle SQL Docs</a></li>
    </ul>
</asp:Content>
