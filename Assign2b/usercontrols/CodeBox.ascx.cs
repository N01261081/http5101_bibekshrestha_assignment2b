﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assign2b
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        List<string> my_code;

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView myCodeDS = MyCodeSource();
            data_grid_code.DataSource = myCodeDS;
            data_grid_code.DataBind();
            
        }

        DataView MyCodeSource()
        {
            DataTable codedata = new DataTable();

            DataColumn id_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            id_col.ColumnName = "Line";
            id_col.DataType = System.Type.GetType("System.Int32");
            codedata.Columns.Add(id_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> my_css_code = new List<string>(new string[]
            {
                ".menu {",
                "~list-style: none;",
                "~padding - left: 0;",
                "}",

                ".hidden {",
                "~position: absolute;",
                "~left: -100000px;",
                "}",

                ".menu li {",
                "~display: inline-block;",
                "~margin-right: 1.5em;",
                "}",

                "#main-menu a{",
                "~font-weight: bold;",
                "~text-decoration: none;",
                "}",

                "#main-menu a, #main-menu a:visited {",
                "~color: #000;",
                "}",

                ".block-title {",
                "~font-weight: normal;",
                "~text-transform: uppercase;",
                "}",

                ".button, .button:visited {",
                "~background-color: #bbb;",
                "~border: 1px solid #000;",
                "~padding: 5px 10px;",
                "~color: #000;",
                "~text-decoration: none;",
                "~border-radius: 5px 20px 0;",
                "}"
            });

            List<string> some_css_code = new List<string>(new string[]
            {
                "p {",
                "~margin: 0;",
                "}",

                "article {",
                "~max-width: 500px;",
                "~padding: 10px;",
                "~background-color: red;",
                "~background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.25));",
                "}",  

                ".simple {",
                "~box-shadow: 5px 5px 5px rgba(0,0,0,0.7);",
                "}"
            });

            List<string> my_db_code = new List<string>(new string[]
            {
                "--implicity syntax (inner join)",
                "SELECT clients.clientfname, clients.clientlname, invoices.invoice_description, invoices.invoice_amount",
                "~from clients, invoices",
                "~where invoices.clientid = clients.clientid;",

                "--explicity sysntax(inner join)",
                "select c.clientid, c.clientfname, c.clientlname, i.invoiceid, i.invoice_description, i.invoice_amount",
                "~from clients c inner join invoices i on c.clientid = i.clientid;",

                "--left outer join or left join",
                "select c.clientid, c.clientfname, c.clientlname, i.invoiceid, i.invoice_description, i.invoice_amount",
                "~from clients c left outer join invoices i on c.clientid = i.clientid;",

                "--Explicit left outer join",
                "select i.clientid, c.clientfname, c.clientlname, i.invoiceid, i.invoice_description, i.invoice_amount",
                "~from clients c left outer join invoices i on c.clientid = i.clientid;",

                "--Implicit left outer join",
                "select i.clientid, c.clientfname, c.clientlname, i.invoiceid, i.invoice_description, i.invoice_amount",
                "~from clients c, invoices i where c.clientid = i.clientid(+);",

                "select clients.clientid, clients.clientfname, clients.clientlname, invoices.invoiceid,",
                "~invoices.invoice_description, invoices.invoice_amount",
                "~from invoices left outer join clients on clients.clientid = invoices.clientid;",

                "select clients.clientid, clients.clientfname, clients.clientlname, invoices.invoiceid, ",
                "~invoices.invoice_description, invoices.invoice_amount",
                "~from invoices right outer join clients on clients.clientid = invoices.clientid;",

                "select clients.clientid, clients.clientfname, clients.clientlname, invoices.invoiceid,",
                "~invoices.invoice_description, invoices.invoice_amount",
                "from clients right outer join invoices on clients.clientid = invoices.clientid;"
            });

            List<string> some_db_code = new List<string>(new string[]
            {
                "SELECT last_name, salary,",
                "~COUNT(*) OVER (ORDER BY salary RANGE BETWEEN 50 PRECEDING AND",
                "~150 FOLLOWING) AS mov_count",
                "~FROM employees",
                "~ORDER BY salary, last_name;"
            });

            List<string> my_js_code = new List<string>(new string[]
            {
                "var valid = false;",
                "while(valid === false){",
                "var userInput = prompt(\"Which top 10 book would you like?\",",
                "~~\"please enter a number between 1 and 10!\");",
                "if(userInput === null || userInput === \"\" || userInput === ",
                "~~\"please enter a number between 1 and 10!\"){",
                "~alert(\"please enter a number between 1 and 10!\");",
                "}else if (userInput > 0 && userInput<11){",
	            "~alert(\"Number \" + userInput + \" on the list is \" + bookList[userInput - 1]);",
                "valid = true;",
                "}else {",
	            "alert(\"please enter a number between 1 and 10!\");",
                "~}",
                "}",
                "for(var i = 0; i<bookList.length; i++){",
                "console.log(\"Book \" + (i+1) + \": \" + bookList[i]);",
                "}"
            });

            List<string> some_js_code = new List<string>(new string[]
            {
                "function findNthPrime(n) {",
                "~var curr = 1;",
                "~var counter = 0;",
                "~var primes = [];",
                "~while (counter<n) {",
                "~~curr++;",
                "~~prime = true;",
                "~~for (var i = 2; i<curr; i++) {",
                "~~~if (curr % i === 0) {",
                "~~~~prime = false;",
                "~~~~break;",
                "~~}",
                "~~}",
                "~~if (prime === true) {",
                "~~~primes.push(curr);",
                "~~~counter++;",
                "~~}",
                "~}",
                "~return primes[n - 1];",
                "}"
            });

            if (Code == "css")
            {
                if (Owner == "me")
                {
                    my_code = my_css_code;
                }
                else
                {
                    my_code = some_css_code;
                }
            }
            else if (Code == "db")
            {
                if (Owner == "me")
                {
                    my_code = my_db_code;
                }
                else
                {
                    my_code = some_db_code;
                }
            }
            else if (Code == "js")
            {
                if (Owner == "me")
                {
                    my_code = my_js_code;
                }
                else
                {
                    my_code = some_js_code;
                }
            }

            int i = 0;

            foreach (string code_line in my_code)
            {
                coderow = codedata.NewRow();
                coderow[id_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;

        }

        public string Owner
        {
            get { return (string)ViewState["Owner"]; }
            set { ViewState["Owner"] = value; }

        }

        public string Code
        {
            get { return (string)ViewState["Code"]; }
            set { ViewState["Code"] = value; }

        }
    }
}