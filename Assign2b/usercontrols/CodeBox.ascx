﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="Assign2b.WebUserControl1" %>
<asp:DataGrid id="data_grid_code" runat="server" CssClass="code"
    GridLines="None" Width="100%" CellPadding="2">
    <HeaderStyle Font-Size="Large" BackColor="#A9A9A9" ForeColor="Black" />
    <ItemStyle BackColor="#F0F8FF" ForeColor="Black" />
</asp:DataGrid>

<asp:Label runat="server" ID="output"></asp:Label>