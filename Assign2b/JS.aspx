﻿<%@ Page Title="JavaScript" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JS.aspx.cs" Inherits="Assign2b.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="trickyConcept" runat="server">
    <h2>Javascript Functions</h2>
    <p>
        A javascript function is a group of codes designed to perfom a specific task. 
        To execute the function, we need to call the function with its name.
    </p>
    <p>
        Functions are independent units of behaviour. Functions can be assigned to variables, stored in objects or arrays,
        passed as an argument to other functions, and returned from functions.
    </p>

    <p>
        There are three ways to define a function:
    </p>

    <ul>
        <li>Function Declaration
            <code class="code-line">function doSomething() {}</code>
        </li>
        <li>Function Expression
            <code class="code-line">let doSomething = function() {}</code>
        </li>
        <li>Arrow Function
            <code class="code-line">let doSomething = () => {}</code>
        </li>
    </ul>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="myCode" runat="server">
    <p>The code below is a part of an assignment. This code checks the input from the user and validates it first.</p>
    <p>
        Valid inputs are numbers from 1 to 10. Depending on the input from the user, 
        this program outputs a book name from a list of my favourite books.
    </p>
    <p>In the end, this code displays all the books in the list in the console.</p>
    <uc:CodeBox ID="my_js_code" runat="server" Code="js" Owner="me" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="someCode" runat="server">
    <p>
        The block of code below returns nth prime number. This code is extracted from 
        <a href="https://medium.com/@atej/learn-javascript-closure-nth-prime-d0cf7bc5ef47" target="_blank">
            Understand JavaScript closure. Once and for all.
        </a>

    </p>
    
    <uc:CodeBox ID="some_js_code" runat="server" Code="js" Owner="other" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="list" runat="server">
    <h3>Useful links</h3>
    <ul>
        <li><a href="https://www.w3schools.com/js/" target="_blank">W3SCHOOLS</a></li>
        <li><a href="https://www.javascript.com/" target="_blank">Javascript.com</a></li>
        <li><a href="https://developer.mozilla.org/bm/docs/Web/JavaScript" target="_blank">Mozilla Developer Network</a></li>
        <li><a href="https://www.codecademy.com/learn/introduction-to-javascript" target="_blank">Codecademy</a></li>
        <li><a href="https://stackoverflow.com/questions/tagged/javascript" target="_blank">StackOverflow</a></li>
        <li><a href="https://medium.freecodecamp.org/learn-these-javascript-fundamentals-and-become-a-better-developer-2a031a0dc9cf" target="_blank">Medium Post</a></li>
    </ul>

</asp:Content>
