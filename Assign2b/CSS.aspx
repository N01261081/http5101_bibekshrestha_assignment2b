﻿<%@ Page Title="CSS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSS.aspx.cs" Inherits="Assign2b.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="trickyConcept" runat="server">
    <h2>CSS float</h2>
    <p>The float property is used for positioning and layout on web pages.</p>
    <p>
        The float CSS property specifies that an element should be along the left or right side of its container, 
        allowing text and inline elements to wrap around it. The element is removed from the normal flow of the web page, 
        though still remaining a part of the flow (in contrast to absolute positioning).
    </p>
    <p>The float property is assigned a value, choosen from the list values below.</p>
    <ul>
        <li>none</li>
        <li>left</li>
        <li>right</li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="myCode" runat="server">
    <p>The code below is a snippet from my assignment for CSS class. It just styles few elements in the content to look good.</p>

    <uc:CodeBox runat="server" id="my_css_code" Code="css" Owner="me" />


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="someCode" runat="server">
    <p>This code block below styles box with advanced effects. It was extracted from 
        <a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_boxes/Advanced_box_effects" target="_blank">
            MDN | Advanced box effects
        </a>
    </p>
    <uc:CodeBox runat="server" id="some_css_code" Code="css" Owner="other" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="list" runat="server">
    <h3>Useful links</h3>
    <ul>
        <li><a href="https://www.w3schools.com/css/" target="_blank">W3SCHOOLS</a></li>
        <li><a href="https://css-tricks.com/" target="_blank">CSS Tricks</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS" target="_blank">Mozilla Developer Network</a></li>
        <li><a href="https://www.codecademy.com/learn/learn-css" target="_blank">Codecademy</a></li>
    </ul>
</asp:Content>
