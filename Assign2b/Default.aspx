﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assign2b._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="trickyConcept" runat="server">
    <h1>Study Guide for MidTerm</h1>
    <div class="container home-page-container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <a href="/CSS">
                    <div class="home-page-block">
                        CSS
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-12">
                <a href="/DB">
                    <div class="home-page-block">
                        Database
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-12">
                <a href="/JS">
                    <div class="home-page-block">
                        Javascript
                    </div>
                </a>
            </div>
        </div>
    </div>

</asp:Content>